package com.example.readingisgood.controller;

import com.example.readingisgood.dao.CustomerRepository;
import com.example.readingisgood.dao.ReadingGenericDao;
import com.example.readingisgood.dto.CustomerDto;
import com.example.readingisgood.exception.RestException;
import com.example.readingisgood.model.Customer;
import com.example.readingisgood.response.CrudResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import org.modelmapper.ModelMapper;
import java.util.List;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;


@RestController
@RequestMapping("/readingIsGood/customers")
@Api(value="ReadingIsGood Customer REST API",tags="customer")
public class CustomerController {

    private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ReadingGenericDao readingGenericDao;

    @Autowired
    private ModelMapper modelMapper;

    @Operation(summary = "Add New Customer", description = "Create a new customer", tags = {"customer"})
    @PostMapping(path="/v1", produces = "application/json")
    public ResponseEntity<CrudResponse> addNewCustomer(
            @Parameter(description = "Customer info to save", required = true) @RequestBody CustomerDto customerDto){
        logger.info("addNewCustomer: {}", customerDto);
        Customer customer = mapToCustomer(customerDto);
        Customer createdCustomer = null;
        try {
            createdCustomer = customerRepository.save(customer);
        } catch (org.springframework.dao.DuplicateKeyException e) {
            e.printStackTrace();
            throw RestException.duplicateEmail(customerDto.getEmail());
        } catch (com.mongodb.MongoWriteException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(new CrudResponse().setId(createdCustomer.getId()), HttpStatus.CREATED);
    }

    @Operation(summary = "Get Customer's Order By Id", description = "Customer order search by customerId", tags = {"customer"})
    @GetMapping(path="/v1/customerOrders/{id}", produces = "application/json")
    public ResponseEntity<List<String>> getCustomerOrdersById(@PathVariable("id") String customerId) {
        logger.info("getCustomerOrdersById : {}", customerId);
        List<String> customerOrders = null;
        try {
            customerOrders = readingGenericDao.findCustomerOrders(customerId);
            if (customerOrders.isEmpty()) {
                logger.info("There is no any order for this customer {}", customerId);
                return new ResponseEntity<>(HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(customerOrders, HttpStatus.OK);
    }

    public Customer mapToCustomer(CustomerDto customerDto) {
        return modelMapper.map(customerDto, Customer.class);
    }

    public CustomerDto mapToCustomerDto(Customer customer) {
        return modelMapper.map(customer, CustomerDto.class);
    }


}

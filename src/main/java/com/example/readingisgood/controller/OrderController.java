package com.example.readingisgood.controller;

import com.example.readingisgood.dao.BookRepository;
import com.example.readingisgood.dao.CustomerRepository;
import com.example.readingisgood.dao.ReadingGenericDao;
import com.example.readingisgood.dao.OrderRepository;
import com.example.readingisgood.dto.OrderDto;
import com.example.readingisgood.model.Book;
import com.example.readingisgood.model.Customer;
import com.example.readingisgood.model.Order;
import com.example.readingisgood.response.CrudResponse;
import io.swagger.annotations.Api;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/readingIsGood/orders")
@Api(value="ReadingIsGood Order REST API",tags="order")
public class OrderController {

    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ReadingGenericDao readingGenericDao;

    @Autowired
    private ModelMapper modelMapper;

    @Operation(summary = "Add New Order", description = "Create a new order", tags = {"order"})
    @PostMapping(path="/v1", produces = "application/json")
    public ResponseEntity<CrudResponse> addNewOrder(@RequestBody OrderDto orderDto){
        logger.info("addNewOrder: {}", orderDto);
        Order order = mapToOrder(orderDto);
        Order createdOrder = null;
        try {
            Book book = bookRepository.findBookById(order.getBookId());
            if (!isBookStockOkForOrder(book)) {
                logger.info("Out of stock. Book Name:{} Book Id:{}", book.getBookName(), book.getId());
                return new ResponseEntity<>(new CrudResponse().setId(null), HttpStatus.PRECONDITION_FAILED);
            }
            createdOrder = orderRepository.save(order);
            if (createdOrder != null) {
                readingGenericDao.updateBookStockSafe(book);

                // update customer's order info
                Customer customer = customerRepository.findCustomerById(createdOrder.getCustomerId());
                addNewOrderToCustomer(customer, createdOrder.getId());

            }
        } catch (org.springframework.dao.DuplicateKeyException e) {
            e.printStackTrace();
        } catch (com.mongodb.MongoWriteException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(new CrudResponse().setId(createdOrder.getId()), HttpStatus.CREATED);
    }

    @Operation(summary = "Get Order By Id", description = "Order search by orderId", tags = {"order"})
    @GetMapping(path = "/v1/{id}", produces = "application/json")
    public ResponseEntity<Order> getOrderById(@PathVariable("id") String orderId) {
        logger.info("getOrderById: {}", orderId);
        Order order = null;
        try {
            order = orderRepository.findOrderById(orderId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.debug("getOrderById: {}", order);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @Operation(summary = "Get Order By Date", description = "Order search between two dates", tags = {"order"})
    @GetMapping(path="/v1/byDate", produces = "application/json")
    public ResponseEntity<List<Order>>  getOrdersByDate(
            @RequestHeader(value = "startDate", required = true) String startDate,
            @RequestHeader(value = "endDate", required = true) String endDate) {
        logger.info("getOrdersByDate : {} {}", startDate, endDate);
        List<Order> orders = null;
        try {
            orders = readingGenericDao.getOrdersByDate(startDate, endDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    private Order mapToOrder(OrderDto orderDto) {
        return modelMapper.map(orderDto, Order.class);
    }

    public void addNewOrderToCustomer(Customer customer, String orderId) {
        List<String> customerOrderIds = customer.getOrderIds();
        if (customerOrderIds == null) {
            customerOrderIds = new ArrayList<>();
        }

        customerOrderIds.add(orderId);
        customer.setOrderIds(customerOrderIds);
        try {
            customerRepository.save(customer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isBookStockOkForOrder(Book book) {
        if (book.getStocksOfBook() >= 1) {
            return true;
        }
        return false;
    }


}

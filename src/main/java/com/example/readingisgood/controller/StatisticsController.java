package com.example.readingisgood.controller;

import com.example.readingisgood.dao.StatRepository;
import com.example.readingisgood.dto.StatisticDto;
import com.example.readingisgood.model.StatCounts;
import com.example.readingisgood.model.Statistic;
import com.example.readingisgood.response.CrudResponse;
import io.swagger.annotations.Api;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;


@RestController
@RequestMapping("/readingIsGood/stat")
@Api(value="ReadingIsGood Statistics REST API",tags="statistics")
public class StatisticsController {

    private static final Logger logger = LoggerFactory.getLogger(StatisticsController.class);

    @Autowired
    StatRepository statRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Operation(summary = "Get Statistics By Date", description = "Statistics search by month and date", tags = {"statistics"})
    @GetMapping(path="/v1", produces = "application/json")
    public ResponseEntity<StatCounts> getStatByMonthAndYear(
            @RequestHeader(value = "month", required = true) String month,
            @RequestHeader(value = "year", required = true) String year) {
        logger.info("getStatByMonthAndYear: {} {}", month, year);
        StatCounts statCounts = null;
        try {
            Statistic statistic = statRepository.findByMonthAndYear(month, year);
            statCounts = statistic.getStatCounts();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(statCounts, HttpStatus.OK);
    }


    @PostMapping(path="/v1", produces = "application/json")
    public ResponseEntity<CrudResponse> addNewStat(@RequestBody StatisticDto statisticDto){
        logger.info("addNewStat: {}", statisticDto);
        Statistic statistic = mapToStatistics(statisticDto);
        Statistic createdStatistic = null;
        try {
            createdStatistic = statRepository.save(statistic);
        } catch (org.springframework.dao.DuplicateKeyException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(new CrudResponse().setId(createdStatistic.getId()), HttpStatus.CREATED);
    }

    private Statistic mapToStatistics(StatisticDto statisticDto) {
        return  modelMapper.map(statisticDto, Statistic.class);
    }

}

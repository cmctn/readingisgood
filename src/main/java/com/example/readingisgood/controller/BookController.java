package com.example.readingisgood.controller;


import com.example.readingisgood.dao.BookRepository;
import com.example.readingisgood.dao.ReadingGenericDao;
import com.example.readingisgood.dto.BookDto;
import com.example.readingisgood.model.Book;
import com.example.readingisgood.response.CrudResponse;
import io.swagger.annotations.Api;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/readingIsGood/books")
@Api(value="ReadingIsGood Book REST API",tags="book")
public class BookController {

    private static final Logger logger = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private ReadingGenericDao readingGenericDao;

    @Autowired
    private ModelMapper modelMapper;

    @Operation(summary = "Add New Book", description = "Create a new book", tags = {"book"})
    @PostMapping(path="/v1", produces = "application/json")
    public ResponseEntity<CrudResponse> addNewBook(@RequestBody BookDto bookDto){
        logger.info("addNewBook: {}", bookDto);
        Book book = mapToBook(bookDto);
        Book createdBook = null;
        try {
            createdBook = bookRepository.save(book);
        } catch (org.springframework.dao.DuplicateKeyException e) {
            e.printStackTrace();
        } catch (com.mongodb.MongoWriteException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(new CrudResponse().setId(createdBook.getId()), HttpStatus.CREATED);
    }

    @Operation(summary = "Update Book's Stock Info", description = "Update stock info for ordering books", tags = {"book"})
    @PutMapping(path="/v1/books/{id}", produces = "application/json")
    public ResponseEntity<BookDto> updateBookStock(@PathVariable("id") String id) {
        logger.info("updateBookStock: {}", id);
        BookDto bookDto = null;
        Book book = bookRepository.findBookById(id);
        Book updatedBook = readingGenericDao.updateBookStockSafe(book);
        bookDto = mapToBookDto(updatedBook);
        return new ResponseEntity<>(bookDto, HttpStatus.OK);
    }


    private Book mapToBook(BookDto bookDto) {
        return modelMapper.map(bookDto, Book.class);
    }

    public BookDto mapToBookDto(Book book) {
        return modelMapper.map(book, BookDto.class);
    }


}
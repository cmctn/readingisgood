package com.example.readingisgood.exception;

import java.util.Formatter;


public class RestException extends RuntimeException {

    private String errorCode;
    private String errorDesc;

    public RestException(String errorCode, String errorDesc) {
        super(errorDesc);
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    /*
    *     public static SubscriptionRestServiceException duplicateMsisdn(String msisdn) {
        Formatter formatter = new Formatter();
        String text = formatter.format("Subscriber [%s] already available", msisdn).toString();
        formatter.close();
        return new SubscriptionRestServiceException(String.valueOf(5), text);
    }
    * */

    public static RestException duplicateEmail(String email) {
        Formatter formatter = new Formatter();
        String text = formatter.format("Email [%s] already available", email).toString();
        formatter.close();
        return new RestException(String.valueOf(5), text);
    }



}

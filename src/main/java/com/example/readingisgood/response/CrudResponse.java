package com.example.readingisgood.response;

public class CrudResponse {

    private String id;

    public CrudResponse() {
    }

    public String getId() {
        return id;
    }

    public CrudResponse setId(String id) {
        this.id = id;
        return this;
    }
}

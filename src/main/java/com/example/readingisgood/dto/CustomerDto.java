package com.example.readingisgood.dto;


import java.util.List;

public class CustomerDto {

    private String id;
    private String customerName;
    private List<String> orderIds;
    private Long version;
    private String email;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public List<String> getOrderIds() {
        return orderIds;
    }

    public void setOrderIds(List<String> orderIds) {
        this.orderIds = orderIds;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "CustomerDto{" +
                "id='" + id + '\'' +
                ", customerName='" + customerName + '\'' +
                ", orderIds=" + orderIds +
                ", version=" + version +
                ", email='" + email + '\'' +
                '}';
    }

}

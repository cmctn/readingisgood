package com.example.readingisgood.dto;

public class BookDto {

    private String id;
    private String bookName;
    private String isbn;
    private String category;
    private String author;
    private Integer stocksOfBook;
    private Long version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getStocksOfBook() {
        return stocksOfBook;
    }

    public void setStocksOfBook(Integer stocksOfBook) {
        this.stocksOfBook = stocksOfBook;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "BookDto{" +
                "id='" + id + '\'' +
                ", bookName='" + bookName + '\'' +
                ", isbn='" + isbn + '\'' +
                ", category='" + category + '\'' +
                ", author='" + author + '\'' +
                ", stocksOfBook=" + stocksOfBook +
                ", version=" + version +
                '}';
    }
}

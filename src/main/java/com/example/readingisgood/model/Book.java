package com.example.readingisgood.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="book")
@CompoundIndexes({
        @CompoundIndex(def = "{'isbn':1}", name = Book.ISBN_IDX, unique=true, background=true, sparse=false)
})
public class Book {

    @Id
    private String id;
    private String bookName;
    private String isbn;
    private String category;
    private String author;
    private Integer stocksOfBook;
    @Version
    private Long version;

    public static final String ISBN_IDX = "isbn_idx";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getStocksOfBook() {
        return stocksOfBook;
    }

    public void setStocksOfBook(Integer stocksOfBook) {
        this.stocksOfBook = stocksOfBook;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id='" + id + '\'' +
                ", bookName='" + bookName + '\'' +
                ", isbn='" + isbn + '\'' +
                ", category='" + category + '\'' +
                ", author='" + author + '\'' +
                ", stocksOfBook=" + stocksOfBook +
                ", version=" + version +
                '}';
    }
}

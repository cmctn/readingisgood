package com.example.readingisgood.model;

public class StatCounts {

    private int totalOrderCount;
    private int totalOrderedBookCount;
    private double totalPurchasedAmount;

    public int getTotalOrderCount() {
        return totalOrderCount;
    }

    public void setTotalOrderCount(int totalOrderCount) {
        this.totalOrderCount = totalOrderCount;
    }

    public int getTotalOrderedBookCount() {
        return totalOrderedBookCount;
    }

    public void setTotalOrderedBookCount(int totalOrderedBookCount) {
        this.totalOrderedBookCount = totalOrderedBookCount;
    }

    public double getTotalPurchasedAmount() {
        return totalPurchasedAmount;
    }

    public void setTotalPurchasedAmount(double totalPurchasedAmount) {
        this.totalPurchasedAmount = totalPurchasedAmount;
    }

    @Override
    public String toString() {
        return "StatCounts{" +
                "totalOrderCount=" + totalOrderCount +
                ", totalOrderedBookCount=" + totalOrderedBookCount +
                ", totalPurchasedAmount=" + totalPurchasedAmount +
                '}';
    }
}

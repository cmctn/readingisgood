package com.example.readingisgood.model;


import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.List;

@Document(collection="customer")
@CompoundIndexes({
        @CompoundIndex(def = "{'email':1}", name = Customer.EMAIL_IDX, unique=true, background=true, sparse=false)
})
public class Customer {

    @Id
    private String id;
    private String customerName;
    private List<String> orderIds;
    @Version
    private Long version;
    private String email;

    public static final String EMAIL_IDX = "email_idx";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public List<String> getOrderIds() {
        return orderIds;
    }

    public void setOrderIds(List<String> orderIds) {
        this.orderIds = orderIds;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id='" + id + '\'' +
                ", customerName='" + customerName + '\'' +
                ", orderIds=" + orderIds +
                ", version=" + version +
                ", email='" + email + '\'' +
                '}';
    }

}

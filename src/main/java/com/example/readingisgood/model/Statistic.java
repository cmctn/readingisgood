package com.example.readingisgood.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;

public class Statistic {

    @Id
    private String id;
    private String month;
    private String year;
    private StatCounts statCounts;
    @Version
    private Long version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public StatCounts getStatCounts() {
        return statCounts;
    }

    public void setStatCounts(StatCounts statCounts) {
        this.statCounts = statCounts;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Statistic{" +
                "id='" + id + '\'' +
                ", month='" + month + '\'' +
                ", year='" + year + '\'' +
                ", statCounts=" + statCounts +
                ", version=" + version +
                '}';
    }
}

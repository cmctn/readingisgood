package com.example.readingisgood.dao;

import com.example.readingisgood.model.Statistic;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatRepository extends MongoRepository <Statistic, String> {

    Statistic findByMonthAndYear(String month, String year);


}

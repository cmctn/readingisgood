package com.example.readingisgood.dao;

import com.example.readingisgood.model.Order;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends MongoRepository <Order, String> {

    Order findOrderById(String id);
}

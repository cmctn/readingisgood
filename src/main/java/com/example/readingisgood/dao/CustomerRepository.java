package com.example.readingisgood.dao;

import com.example.readingisgood.model.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends MongoRepository<Customer, String> {

    Customer findCustomerById(String id);

    Customer findCustomerByCustomerName(String customerName);

    void deleteCustomerById(String id);

    void deleteCustomerByCustomerName(String customerName);

}

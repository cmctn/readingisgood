package com.example.readingisgood.dao;

import com.example.readingisgood.model.Book;
import com.example.readingisgood.model.Customer;
import com.example.readingisgood.model.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class ReadingGenericDao {

    @Autowired
    MongoOperations mongoOperations;

    private static final Logger logger = LoggerFactory.getLogger(ReadingGenericDao.class);

    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    public List<String> findCustomerOrders(String customerId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(customerId));
        Customer customer = mongoOperations.findOne(query, Customer.class);
        List<String> orderIds = customer.getOrderIds();
        return orderIds;
    }

    public Book updateBookStockSafe(Book book) {
        Integer stocksOfBook = book.getStocksOfBook();
        book.setStocksOfBook(stocksOfBook -1);

        try {
            mongoOperations.save(book);
        } catch (org.springframework.dao.OptimisticLockingFailureException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return book;
    }

    public List<Order> getOrdersByDate(String startDate, String endDate) {
        List<Order> orders = null;
        try {
            Query query = new Query();
            Date start = new SimpleDateFormat(DATE_FORMAT).parse(startDate);
            Date end = new SimpleDateFormat(DATE_FORMAT).parse(endDate);
            query.addCriteria(Criteria.where("orderDate").gte(start).lt(end));
            orders = mongoOperations.find(query, Order.class);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return orders;
    }


}

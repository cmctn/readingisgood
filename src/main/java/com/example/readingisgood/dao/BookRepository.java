package com.example.readingisgood.dao;

import com.example.readingisgood.model.Book;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends MongoRepository <Book, String> {

    Book findBookById(String id);


}

ReadingIsGood Application
---------------
This application provides Rest APIs for online books retails.

### Tech Stack
- JDK 11
- Spring Boot
- MongoDB
- Maven
- Docker

### How to Run Application
The following steps should be taken on terminal:

- Go into the project path which docker-compose.yml and pom.xml exists  
- `mvn clean install`
- `docker-compose build`
- `docker-compose up`

`docker-compose down` command should be used to stop the application properly.

In case of any unproper docker-compose stop, you will need to run `docker volume prune` command.  

### Bearer Token
Token is necessary to access Rest APIs. `Get Access Token` request is provided in postman collection. 